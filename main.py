from src import expe
from src.dataset import Dataset


res = expe.launch_expe(
    df_clients=Dataset.from_csv("./csv/daily_base.csv"),
    clients_name="daily_base_150",
    agg_size=10,
    method="rng-0",
    method_params=0,
    days_name="num-start-1",
    days_params=1,
    threads=None,
    wall_time=100,
    pooled=True
)
res.to_csv("res.csv", index=False)
print(res)
