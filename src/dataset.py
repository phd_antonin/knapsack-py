import pandas as pd
import random
#import pydp as dp
#from pydp.algorithms.laplacian import BoundedSum

class Dataset:

    def __init__(self, df):
        self.df: pd.DataFrame = df

    def from_csv(path):
        df = pd.read_csv(path)

        if "Unnamed: 0" in df:  # Might be present if the index wasn't removed in the clients file, useless
            df = df.drop("Unnamed: 0", axis=1)

        return Dataset(df)

    def from_parquet(path):
        df = pd.read_parquet(path)

        if "Unnamed: 0" in df:  # Might be present if the index wasn't removed in the clients file, useless
            df = df.drop("Unnamed: 0", axis=1)

        return Dataset(df)

    def clients(self):
        return list(self.df.columns[1:])

    def clients_size(self):
        return len(self.df.columns) - 1

    def get_day(self, day):
        return self.df[self.df['d'] == day]

    def get_subset_days(self, days):
        return Dataset(self.df[self.df['d'].isin(days)])

    def get_subset_array(self, clients):
        clients = ["d"] + clients
        return Dataset(self.df[clients])

    def get_random_subset(self, nclients, seed):
        assert len(self.df.columns) >= nclients  # Avoid being in an infinite loop

        random.seed(seed)

        columns = set()

        while len(columns) < nclients:
            column = random.randint(1, len(self.df.columns) - 1)
            column = int(self.df.columns[column])
            columns.add(column)

        columns = sorted(columns)
        columns = [str(c) for c in columns]

        return self.get_subset_array(columns)

    def filter_outliers(self, quantile):
        """
        Filter the outliers from the dataset.
        All values above the given quantile are set to the value of the element of the given quantile.
        """
        cols = self.df.columns[1:]
        hf = max(self.df.quantile(quantile))

        m = self.df[cols] > hf

        dfn = self.df.where(~m, other=hf)
        dfn["d"] = self.df["d"]
        return Dataset(dfn)

    def get_max(self):
        """
        Get the maximum consumption in the dataset
        """
        return max(self.df.max()[1:])

    def aggregate(self, aggregate="sum"):
        """
        Compute the aggregate for each days of all the consumption in the dataset.
        By default, the aggregate will be "sum"
        aggregate type:
          - "sum" : Sum the values
          - "avg" : Average the values
        """
        df_agg = pd.DataFrame()
        df_agg['d'] = self.df['d']
        df_agg['ukn_n'] = 0
        df_agg['ukn_min'] = None
        df_agg['ukn_max'] = None

        if type == "avg":
            df_agg['agg'] = self.df[self.df.columns[1:]].agg("mean", axis=1)
        else:
            df_agg['agg'] = self.df[self.df.columns[1:]].agg("sum", axis=1)

        return Dataset(df_agg)
