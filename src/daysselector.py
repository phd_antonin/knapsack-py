import random

from src.dataset import Dataset


def rng_days(df_clients, ndays, seed):
    """
    Get fully random days
    """
    random.seed(seed)
    len_days = len(df_clients.df['d'])

    days = set()

    while len(days) < ndays:
        day = random.randint(1, len_days - 1)
        day = df_clients.df['d'][day]
        days.add(day)

    days = list(days)
    days.sort()

    return days


def rng_days_follow(df_clients, ndays, seed):
    """
    Get days that follow each other with a random starting point.
    """
    random.seed(seed)

    total = len(df_clients.df['d']) - ndays
    start = random.randint(0, total)

    return df_clients.df['d'][start: start + ndays]


def build_days(df_clients, days_name, days_parameters=None):
    if days_name == "all":  # All days
        return df_clients.df["d"]

    if days_name[:9] == "all-minus":  # All days without the number in the parameters
        return df_clients.df["d"][days_parameters:]

    if days_name[:9] == "num-start":  # The parameters first days
        return df_clients.df["d"][:days_parameters]

    if days_name[:3] == "rng":  # Random (with the number of days and an optional seed set in the parameters)
        return rng_days(
            df_clients,
            days_parameters['ndays'],
            days_parameters['seed']
        )

    if days_name[:6] == 'st-rng':  # Following days but with the starting point randomly set
        # (with the number of days and an optional seed set in the parameters)
        return rng_days_follow(
            df_clients,
            days_parameters['ndays'],
            days_parameters['seed']
        )

    if days_name[:3] == "set":  # An array of selected days
        return days_parameters

    print("Error in days selection :", days_name, str(days_parameters))
    exit(-1)


if __name__ == "__main__":
    df_clients = Dataset.from_csv("./csv/daily_base.csv")

    print(build_days(df_clients, "all"))


