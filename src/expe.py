import os
import time
import uuid

import pandas as pd
import numpy as np
from src import subset, daysselector, check, aggregate
from src.dataset import Dataset
from src.gurobi import KsGurobi


def build_results_dataframe():
    """
    Build an empty dataframe with all the columns set. ready to receive the results of the experiments
    """
    return pd.DataFrame(columns=[
        "expe",  # Name of the experiment (folder name)
        "clients",  # Clients name
        "clients_size",  # Number of clients selected
        "clients_subset",  # Client subset selection method
        "agg_size",  # Aggregate size
        "agg_size_pc",  # Aggregate size (in percentage of the total number of clients)
        "method",  # Aggregate subset selection method
        "aggregate",  # Aggregate computation method (sum or avg)
        "days",  # Days selection method
        "days_size",  # Number of days
        "days_total",  # Total number of days present in the dataset
        "noise_method",  # Noise method used
        "time",  # Computation time
        "valid",  # Is the result valid ?
        "valid_sum",  # Is the solution sums found match the aggregate sums
        "success",  # Is the attack a success (1 solution and no wall hit)
        "score_avg",  # Average score on all solutions
        "score_min",  # Minimal score on all solutions
        "score_max",  # Maximal score on all solutions
        "nsols",  # Number of solutions found (up to 2)
        "oracle",  # The real solution
        "wall_hit",  # Is the wall time hit ?
        "wall_time",  # Wall time value
        "pooled",  # Search for two results
        "pool_size",  # Number of solutions to search
        "uuid",  # Attack unique id
    ])


def launch_expe(expe_name, df_clients, clients_name, clients_subset, agg_size,
                method, method_params, days_name, days_params, noise_name, noise_method, noise_params,
                noise_aggregate, threads, wall_time, pooled, pool_size, uid):
    """
    Launch an unique experiment for a given set of clients, an aggregate size and a set of days.
    Return the time needed for the solver to get a solution and if this solution is valid.
    """
    # Select days to be put on the agg
    days_selected = daysselector.build_days(df_clients, days_name, days_params)

    df_subset = subset.build_subset(df_clients, agg_size, method, method_params)
    df_subset = df_subset.get_subset_days(days_selected)

    df_clients, df_subset, df_agg = aggregate.build_aggregate(df_clients, df_subset, noise_method, noise_aggregate,
                                                              noise_params)

    #print(df_clients.df.head())
    #print(df_subset.df.head())
    #print(df_agg.df.head())
    #exit(-1)



    start = time.time()
    ks = KsGurobi(df_clients, df_agg, days_selected, agg_size, threads, wall_time, pooled, pool_size)
    solutions, ukns = ks.optimize()
    elapsed = time.time() - start

    valid, valid_sums, score_avg, score_min, score_max = check.check_solutions(solutions, df_clients, df_subset, df_agg, days_selected)

    nsols = ks.get_nsols()
    hit = ks.hit_wall_time()
    optimal = ks.is_optimal()
    success = (nsols == 1) and not hit

    #print("SOLS : ")
    #print(solutions)
    print("UKNS : ")
    print(ukns)
    print("OPTIMAL :", optimal)
    print("NSOLS :", nsols)

    df_results = build_results_dataframe()
    df_results = df_results.append({
        "expe": expe_name,
        "clients": clients_name,
        "clients_size": df_clients.clients_size(),
        "clients_subset": clients_subset,
        "agg_size": agg_size,
        "agg_size_pc": int(agg_size / df_clients.clients_size() * 100),
        "method": method,
        "aggregate": noise_aggregate,
        "days": days_name,
        "days_size": len(days_selected),
        "days_total": len(df_clients.df['d']),
        "noise_method": noise_name,
        "time": elapsed,
        "valid": valid,
        "valid_sums": valid_sums,
        "success": success,
        "score_avg": score_avg,
        "score_min": score_min,
        "score_max": score_max,
        "nsols": nsols,
        "oracle": list(df_subset.df.columns[1:]),
        "optimal": optimal,
        "wall_hit": hit,
        "wall_time": wall_time,
        "pooled": pooled,
        "pool_size": pool_size,
        "infeasible": ks.is_infeasible(),
        "uuid": uid
    }, ignore_index=True)

    if len(solutions) == 0:
        solutions = [None]

    return df_results, solutions


def single_expe(expe_name, clients_path, clients_name, clients_subset, clients_param, clients_size, agg_size,
                method, method_params, days_name, days_params, noise_name, noise_method, noise_params, noise_aggregate,
                threads, wall_time, pooled, pool_size, path_out, path_sols):
    """
    Launch a single gurobi experiment and write the results in a CSV file.
    If the file already exist, the results will be added to the existing file.
    """
    # Attack unique id
    uid = uuid.uuid4().hex[:10]

    print("[-] Loading CSV")
    if ".parquet" in clients_path:
        df_clients = Dataset.from_parquet(clients_path)
    else:
        df_clients = Dataset.from_csv(clients_path)
    print("[-] CSV Loaded")


    df_clients = subset.build_subset(df_clients, clients_size, clients_subset, clients_param)

    print("[-] Subset")

    df_results, sols = launch_expe(expe_name, df_clients, clients_name, clients_subset, agg_size,
                             method, method_params, days_name, days_params, noise_name, noise_method, noise_params,
                             noise_aggregate, threads, wall_time, pooled, pool_size, uid)

    if os.path.isfile(path_out):
        df_results.to_csv(path_out, mode="a", header=False, index=False)
    else:
        df_results.to_csv(path_out, header=True, index=False)

    vsols = np.vstack(sols)

    path_sol = os.path.join(path_sols, uid + ".npy")

    with open(path_sol, "wb") as f:
        np.save(f, vsols)