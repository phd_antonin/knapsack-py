import gurobipy as gp

from src.dataset import Dataset


class KsGurobi:

    def __init__(self, df_clients: Dataset, df_agg: Dataset, days: [str], nvar, threads, wall_time, pooled, pool_size):
        self.df_clients = df_clients
        self.df_agg = df_agg
        self.days = days
        self.nvar = nvar
        self.threads = threads
        self.wall_time = wall_time

        self.ukns = dict()

        self.variables = dict()
        self.objective = False

        self.model = gp.Model("grb_model")

        if self.threads is not None:
            self.model.setParam(gp.GRB.Param.Threads, self.threads)

        if self.wall_time is not None:
            self.model.setParam(gp.GRB.Param.TimeLimit, self.wall_time)

        # Pool search
        if pooled:
            self.model.setParam(gp.GRB.Param.PoolSolutions, pool_size)  # Keep 2 solutions
            self.model.setParam(gp.GRB.Param.PoolSearchMode, 2)  # Try to get fill that 2 solutions

        self._create_vars()

        for day in days:
            self._constraint_day(day)

        self._constraint_nvar()

        self.model.write("model.lp")

    def _create_vars(self):
        for c in self.df_clients.df.columns[1:]:
            if c not in self.variables:
                v = self.model.addVar(vtype=gp.GRB.BINARY, name=c)
                self.variables[c] = v

    def _constraint_nvar(self):
        expr = None

        for v in self.variables:
            var = self.variables[v]

            if expr is None:
                expr = var
            else:
                expr = expr + var

        self.model.setObjective(expr, gp.GRB.MAXIMIZE)

        expr = expr == self.nvar
        self.model.addConstr(expr, "nvar")

    def _constraint_day(self, day):
        clients_day = self.df_clients.get_day(day)
        agg_day = self.df_agg.get_day(day)

        agg = int(agg_day['agg'])
        ukn_n = int(agg_day['ukn_n'])
        ukn_min = agg_day['ukn_min']
        ukn_max = agg_day['ukn_max']
        expr = None

        for client in self.variables:
            var = self.variables[client]
            conso = int(clients_day[client])

            if expr is None:
                expr = var * conso
            else:
                expr = expr + var * conso

        # Append unknown parameters (noise, unknown clients)
        for i in range(ukn_n):
            name = "u_" + day + "_" + str(i)

            if ukn_min.isnull().bool() and ukn_max.isnull().bool():
                var = self.model.addVar(vtype=gp.GRB.INTEGER, name=name)
            elif ukn_min.isnull().bool() and not ukn_max.isnull().bool():
                var = self.model.addVar(vtype=gp.GRB.INTEGER, ub=int(ukn_max), name=name)
            elif not ukn_min.isnull().bool() and ukn_max.isnull().bool():
                var = self.model.addVar(vtype=gp.GRB.INTEGER, lb=int(ukn_min), name=name)
            else:
                var = self.model.addVar(vtype=gp.GRB.INTEGER, lb=int(ukn_min), ub=int(ukn_max), name=name)

            if day not in self.ukns:
                self.ukns[day] = {}
            self.ukns[day][name] = var

            expr = expr + var

        expr = expr == agg

        self.model.addConstr(expr, day)

    def get_optimal_solution(self):
        solution = []

        for v in self.variables:
            var = self.model.getVarByName(v)
            # print('\t%s %g %g' % (var.varName, var.x, var.x == 1))
            # print(var.x)

            if var.x > 0.5:  # Should be 1 or 0 be it seems it is not always the case (ex : 0,999999)
                solution.append(var.varName)

        # print('Obj: %g' % self.model.objVal)

        return solution

    def get_all_solutions(self):
        sols = []

        for i in range(self.get_nsols()):
            self.model.setParam(gp.GRB.Param.SolutionNumber, i)

            sol = []
            for v in self.variables:
                var = self.model.getVarByName(v)

                if var.Xn > 0.5:
                    sol.append(var.varName)

            sols.append(sol)

        return sols

    def get_all_unknowns(self):
        all_ukns = []

        for i in range(self.get_nsols()):
            self.model.setParam(gp.GRB.Param.SolutionNumber, i)

            ukns = []

            for day in self.ukns:
                for v in self.ukns[day]:
                    var = self.model.getVarByName(v)
                ukns.append(var.x)

            all_ukns.append(ukns)

        return all_ukns

    def get_unknown(self):
        ukns = []

        for day in self.ukns:
            for v in self.ukns[day]:
                var = self.model.getVarByName(v)
            ukns.append(var.x)

        return ukns

    def optimize(self):
        self.model.optimize()

        sols = self.get_all_solutions()
        ukns = self.get_all_unknowns()

        return sols, ukns

    def get_nsols(self):
        return self.model.SolCount

    def hit_wall_time(self):
        return self.model.Status == gp.GRB.TIME_LIMIT

    def is_optimal(self):
        return self.model.Status == gp.GRB.OPTIMAL

    def is_infeasible(self):
        return self.model.Status == gp.GRB.INFEASIBLE

